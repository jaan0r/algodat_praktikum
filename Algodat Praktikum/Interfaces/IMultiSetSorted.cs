﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algodat_Praktikum
{
    interface IMultiSetSorted : ISetSorted
    {
        int[] MultiSetSortedArray();

        List<int> MultiSetSortedLinkedList();
    }
}
